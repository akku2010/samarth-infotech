import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';

@IonicPage()
@Component({
  selector: 'page-stoppages-repo',
  templateUrl: 'stoppages-repo.html',
})
export class StoppagesRepoPage implements OnInit {

  stoppagesReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices: any;
  portstemp: any = [];
  Stoppagesdata: any = [];
  datetime: number;
  selectedVehicle: any;
  vehicleData: any;
  locationEndAddress: any;
  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallStoppages: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ngOnInit() {
    if (this.vehicleData == undefined) {
      this.getdevices();
    } else {
      this.Ignitiondevice_id = this.vehicleData._id;
      // this.Ignitiondevice_id.push(this.vehicleData._id);
      this.getStoppageReport();
    }
  }

  getdevices() {
    var baseURLp = this.apicallStoppages.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicallStoppages.startLoading().present();
    this.apicallStoppages.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicallStoppages.stopLoading();
          console.log(err);
        });
  }

  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.Stoppagesdata);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
      self.file.writeFile(self.file.dataDirectory, "stoppage_report_download.xlsx", blob, {replace:true})
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }


  getStoppagesdevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle._id;

    // this.Ignitiondevice_id = [];
    // if (selectedVehicle.length > 0) {
    //   if (selectedVehicle.length > 1) {
    //     for (var t = 0; t < selectedVehicle.length; t++) {
    //       this.Ignitiondevice_id.push(selectedVehicle[t]._id)
    //     }
    //   } else {
    //     this.Ignitiondevice_id.push(selectedVehicle[0]._id)
    //   }
    // } else return;
  }

  getStoppageReport() {
    if (this.Ignitiondevice_id === undefined) {
      this.Ignitiondevice_id = '';
    }
    let that = this;
    this.apicallStoppages.startLoading().present();
    this.apicallStoppages.getStoppageApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallStoppages.stopLoading();
        this.stoppagesReport = data;
        this.Stoppagesdata = [];
        if (this.stoppagesReport.length > 0) {
          this.innerFunc(this.stoppagesReport);
        } else {
          let toast = this.toastCtrl.create({
            message: "Report(s) not found for selected dates/vehicle.",
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallStoppages.stopLoading();
        console.log(error);
      })
  }

  innerFunc(stoppagesReport) {
    let outerthis = this;
    outerthis.locationEndAddress = undefined;
    var i = 0, howManyTimes = stoppagesReport.length;
    function f() {
      var fd = new Date(outerthis.stoppagesReport[i].arrival_time).getTime();
      var td = new Date(outerthis.stoppagesReport[i].departure_time).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs : ' + rminutes + 'mins';

      outerthis.Stoppagesdata.push({
        'arrival_time': outerthis.stoppagesReport[i].arrival_time,
        'departure_time': outerthis.stoppagesReport[i].departure_time,
        'Durations': Durations,
        'device': outerthis.stoppagesReport[i].device ? outerthis.stoppagesReport[i].device.Device_Name : 'N/A',
        'end_location': {
          'lat': outerthis.stoppagesReport[i].lat,
          'long': outerthis.stoppagesReport[i].long
        }
      });

      outerthis.end_address(outerthis.Stoppagesdata[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  // end_address(item, index) {
  //   let that = this;
  //   that.Stoppagesdata[index].EndLocation = "N/A";
  //   if (!item.end_location) {
  //     that.Stoppagesdata[index].EndLocation = "N/A";
  //   } else if (item.end_location) {
  //     this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
  //       .then((res) => {
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
  //         that.Stoppagesdata[index].EndLocation = str;
  //       })
  //   }
  // }

  end_address(item, index) {
    let that = this;
    if (!item.end_location) {
      that.Stoppagesdata[index].EndLocation = "N/A";
      return;
    }
    let tempcord =  {
      "lat" : item.end_location.lat,
      "long": item.end_location.long
    }
    this.apicallStoppages.getAddress(tempcord)
      .subscribe(res=> {
      console.log("test");
      console.log("endlocation result", res);
      console.log(res.address);
      // that.allDevices[index].address = res.address;
      if(res.message == "Address not found in databse")
      {
        this.geocoderApi.reverseGeocode(item.end_location.lat, item.end_location.long)
       .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
        that.Stoppagesdata[index].EndLocation = str;
        // console.log("inside", that.address);
      })
      } else {
        console.log("enter in else")
        that.Stoppagesdata[index].EndLocation = res.address;
      }
    })
  }
  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicallStoppages.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }
}
