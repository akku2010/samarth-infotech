import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';

@IonicPage()
@Component({
  selector: 'page-trip-report',
  templateUrl: 'trip-report.html',
})
export class TripReportPage implements OnInit {
  islogin: any;
  devices: any;
  portstemp: any = [];
  isdevice: string;
  device_id: any = [];
  isdeviceTripreport: string;
  datetimeStart: string;
  datetimeEnd: string;
  TripReportData: any[] = [];
  TripsdataAddress: any[];
  deviceId: any;
  distanceBt: number;
  StartTime: string;
  Startetime: string;
  Startdate: string;
  EndTime: string;
  Endtime: string;
  Enddate: string;
  datetime: number;
  did: any;
  vehicleData: any;
  locationEndAddress: any;
  locationAddress: any;
  allData: any = {};
  mapData: any[];
  selectedVehicle: any;

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ngOnInit() {
    if (this.vehicleData == undefined) {
      this.getdevices();
    } else {
      // this.device_id = this.vehicleData._id;
      this.device_id.push(this.vehicleData._id);
      this.getTripReport()
    }
  }

  getdevices() {
    var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalligi.startLoading().present();
    this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
        this.isdevice = localStorage.getItem('devices1243');
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.TripReportData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
      self.file.writeFile(self.file.dataDirectory, "trip_report_download.xlsx", blob, {replace:true})
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  getTripdevice(selectedVehicle) {
    // this.device_id = item._id;
    // console.log("device id=> " + this.device_id);

    this.device_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.device_id.push(selectedVehicle[t]._id)
        }
      } else {
        this.device_id.push(selectedVehicle[0]._id)
      }
    } else return;

    this.did = selectedVehicle.Device_ID;
    localStorage.setItem('devices_id', selectedVehicle);
    this.isdeviceTripreport = localStorage.getItem('devices_id');
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getTripReport() {
    this.TripReportData = [];
    if (this.datetimeEnd <= this.datetimeStart && this.device_id) {
      let alert = this.alertCtrl.create({
        message: 'To time is always greater than From Time',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.apicalligi.startLoading().present();
      this.apicalligi.trip_detailCall(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.device_id)
        .subscribe(data => {
          this.apicalligi.stopLoading();
          this.TripsdataAddress = [];
          if (data.length > 0) {
            this.tripFunction(data)
          } else {
            let toast = this.toastCtrl.create({
              message: "Report(s) not found for selected dates/Vehicles.",
              duration: 1500,
              position: "bottom"
            })
            toast.present();
          }
        },
          err => {
            this.apicalligi.stopLoading();
            console.log(err);
          });
    }
  }

  tripFunction(data) {
    let that = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      var deviceId = data[i]._id;
      var distanceBt = data[i].distance / 1000;

      var gmtDateTime = moment.utc(JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate = moment.utc(JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Startetime = gmtDateTime.local().format(' h:mm a');
      var Startdate = gmtDate.format('ll');
      var gmtDateTime1 = moment.utc(JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate1 = moment.utc(JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Endtime = gmtDateTime1.local().format(' h:mm a');
      var Enddate = gmtDate1.format('ll');

      var startDate = new Date(data[i].start_time).toLocaleString();
      var endDate = new Date(data[i].end_time).toLocaleString();

      var fd = new Date(startDate).getTime();
      var td = new Date(endDate).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
      that.TripReportData.push(
        {
          'Device_Name': data[i].device.Device_Name,
          'Device_ID': data[i].device.Device_ID,
          'Startetime': Startetime,
          'Startdate': Startdate,
          'Endtime': Endtime,
          'Enddate': Enddate,
          'distance': distanceBt,
          '_id': deviceId,
          'start_time': data[i].start_time,
          'end_time': data[i].end_time,
          'duration': Durations,
          'end_location': {
            'lat': data[i].end_lat,
            'long': data[i].end_long
          },
          'start_location': {
            'lat': data[i].start_lat,
            'long': data[i].start_long
          }
        });

      that.start_address(that.TripReportData[i], i);
      that.end_address(that.TripReportData[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }

  // start_address(item, index) {
  //   let that = this;
  //   that.TripReportData[index].StartLocation = "N/A";
  //   if (!item.start_location) {
  //     that.TripReportData[index].StartLocation = "N/A";
  //   } else if (item.start_location) {
  //     this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
  //       .then((res) => {
  //         console.log("test", res)
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
  //         that.TripReportData[index].StartLocation = str;
  //       })
  //   }
  // }

  start_address(item, index) {
    let that = this;
      if (!item.start_location) {
        that.TripReportData[index].StartLocation = "N/A";
        return;
      }
      let tempcord =  {
        "lat" : item.start_location.lat,
        "long": item.start_location.long
      }
      this.apicalligi.getAddress(tempcord)
        .subscribe(res=> {
        console.log("test");
        console.log("result", res);
        console.log(res.address);
        // that.allDevices[index].address = res.address;
        if(res.message == "Address not found in databse")
        {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
         .then(res => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          that.TripReportData[index].StartLocation = str;
          // console.log("inside", that.address);
        })
        } else {
          that.TripReportData[index].StartLocation = res.address;
        }
      })
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalligi.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  // end_address(item, index) {
  //   let that = this;
  //   that.TripReportData[index].EndLocation = "N/A";
  //   if (!item.end_location) {
  //     that.TripReportData[index].EndLocation = "N/A";
  //   } else if (item.end_location) {
  //     this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
  //       .then((res) => {
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
  //         that.TripReportData[index].EndLocation = str;
  //       })
  //   }
  // }

    end_address(item, index) {
      let that = this;
      if (!item.end_location) {
        that.TripReportData[index].EndLocation = "N/A";
        return;
      }
      let tempcord =  {
        "lat" : item.end_location.lat,
        "long": item.end_location.long
      }
      this.apicalligi.getAddress(tempcord)
        .subscribe(res=> {
        console.log("test");
        console.log("endlocation result", res);
        console.log(res.address);
        // that.allDevices[index].address = res.address;
        if(res.message == "Address not found in databse")
        {
          this.geocoderApi.reverseGeocode(item.end_location.lat, item.end_location.long)
         .then(res => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
          that.TripReportData[index].EndLocation = str;
          // console.log("inside", that.address);
        })
        } else {
          console.log("enter in else")
          that.TripReportData[index].EndLocation = res.address;
        }
      })
  }

  tripReview(tripData) {
    this.navCtrl.push('TripReviewPage', {
      params: tripData,
      device_id: this.did
    })
  }

}
