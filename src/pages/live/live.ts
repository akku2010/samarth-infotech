import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, ModalController, Platform, ViewController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import { MarkerCluster, GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, ILatLng, Polygon, GoogleMapsAnimation, Polyline } from '@ionic-native/google-maps';
import { TranslateService } from '@ngx-translate/core';
import { DrawerState } from 'ion-bottom-drawer';
import { Subscription } from 'rxjs/Subscription';
import { PoiPage } from '../live-single-device/live-single-device';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import * as moment from 'moment';
declare var google;

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live.html',
})
export class LivePage implements OnInit, OnDestroy {

  // countFlag: number = 0;

  navigateButtonColor: string = '#4369C0'; //Default Color
  navColor: string = '#fff';

  policeButtonColor: string = '#4369C0';
  policeColor: string = '#fff';

  petrolButtonColor: string = "#4369C0";
  petrolColor: string = '#fff';

  shouldBounce = true;
  dockedHeight = 80;
  distanceTop = 200;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 50;
  transition = '0.85s ease-in-out';

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  resToken: string;
  liveDataShare: any;
  isEnabled: boolean = false;
  showMenuBtn: boolean = false;
  latlngCenter: any;
  mapHideTraffic: boolean = false;
  mapData: any = [];
  last_ping_on: any;
  geodata: any = [];
  geoShape: any = [];
  generalPolygon: any;
  locations: any = [];
  acModel: any;
  locationEndAddress: any;
  tempaddress: any;
  mapKey: string;
  shwBckBtn: boolean = false;
  recenterMeLat: any;
  recenterMeLng: any;
  menuActive: boolean;
  deviceDeatils: any = {};
  impkey: any;
  showaddpoibtn: boolean = false;
  isSuperAdmin: boolean;
  isDealer: boolean;
  zoomLevel: number = 18;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public plt: Platform,
    private viewCtrl: ViewController,
    private translate: TranslateService,
    private toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider
  ) {
    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == this.translate.instant('Hybrid')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == this.translate.instant('Normal')) {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == this.translate.instant('Terrain')) {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == this.translate.instant('Satellite')) {
        this.mapKey = 'MAP_TYPE_HYBRID';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));
    this.menuActive = false;
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = {
    "car": this.carIcon,
    "bike": this.carIcon,
    "truck": this.carIcon,
    "bus": this.carIcon,
    "user": this.carIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon,
    "ambulance": this.carIcon
  }
  goBack() {
    this.navCtrl.pop({ animate: true, direction: 'forward' });
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }

  refreshMe() {
    this.ngOnDestroy();
    this.ngOnInit();
  }

  refreshMe1() {
    let that = this;
    this.ngOnDestroy();
    this._io = io.connect(this.apiCall.socketUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    var paramData = that.data;
    this.socketInit(paramData);
  }

  resumeListener: Subscription = new Subscription();
  ionViewWillEnter() {
    if (this.plt.is('ios')) {
      this.shwBckBtn = true;
      this.viewCtrl.showBackButton(false);
    }

    this.plt.ready().then(() => {
      this.resumeListener = this.plt.resume.subscribe(() => {
        var today, Christmas;
        today = new Date();
        Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
        var diffMs = (today - Christmas); // milliseconds between now & Christmas
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        if (diffMins >= 5) {
          localStorage.removeItem("backgroundModeTime");
          this.alertCtrl.create({
            message: this.translate.instant("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
            buttons: [
              {
                text: this.translate.instant('YES PROCEED'),
                handler: () => {
                  // this.getdevices();
                }
              },
              {
                text: this.translate.instant('Back'),
                handler: () => {
                  this.navCtrl.setRoot('DashboardPage');
                }
              }
            ]
          }).present();
        }
      })
    })
  }

  ionViewWillLeave() {
    this.plt.ready().then(() => {
      this.resumeListener.unsubscribe();
    })
  }
  newMap() {
    let mapOptions = {
      controls: {
        zoom: false,
        myLocation: true,
        myLocationButton: false,
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      mapType: this.mapKey
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.plt.is('android')) {
      // map.animateCamera({
      //   target: { lat: 20.5937, lng: 78.9629 },
      //   duration: 500,
      //   // padding: 10,  // default = 20px
      // })
      map.setPadding(20, 20, 20, 20);
    }
    return map;
  }

  navigateFromCurrentLoc() {
    // this.ngOnDestroy();
    if (this.navigateButtonColor == '#4369C0') {
      this.navigateButtonColor = '#f4f4f4';
      this.navColor = '#4369C0';
      // this.apiCall.startLoading().present();
      this.allData.map.getMyLocation().then((location) => {
        var myLocCoords = new LatLng(location.latLng.lat, location.latLng.lng);
        var vehLocCoords = new LatLng(this.recenterMeLat, this.recenterMeLng);
        this.calcRoute(myLocCoords, vehLocCoords);
      });
    } else {
      if (this.navigateButtonColor == '#f4f4f4') {
        this.navigateButtonColor = '#4369C0';
        this.navColor = '#fff';
        if (this.polyLines.length !== 0) {
          // this.polyLines.remove();
          for (var w = 0; w < this.polyLines.length; w++) {
            this.polyLines[w].remove();
          }
        }
      }
    }
  }

  calcRoute(start, end) {
    this.allData.AIR_PORTS = [];
    var directionsService = new google.maps.DirectionsService();
    let that = this;
    var request = {
      origin: start,
      destination: end,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function (response, status) {

      if (status == google.maps.DirectionsStatus.OK) {
        var path = new google.maps.MVCArray();
        for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
          path.push(response.routes[0].overview_path[i]);

          that.allData.AIR_PORTS.push({
            lat: path.g[i].lat(), lng: path.g[i].lng()
          });
          // debugger
          if (that.allData.AIR_PORTS.length > 1) {
            // that.allData.map.addMarker({
            //   title: 'My Location',
            //   position: start,
            //   icon: 'green'
            // }).then((mark: Marker) => {
            //   that.marksArray.push(mark);
            // })
            // that.allData.map.addMarker({
            //   title: 'Destination',
            //   position: end,
            //   icon: 'red'
            // }).then((mark: Marker) => {
            //   that.marksArray.push(mark);
            // })
            that.allData.map.addPolyline({
              'points': that.allData.AIR_PORTS,
              'color': '#4aa9d5',
              'width': 4,
              'geodesic': true,
            }).then((poly: Polyline) => {
              that.polyLines.push(poly);
              that.getTravelDetails(start, end);

              //   that.showBtn = true;
            })
          }
        }

      }
    });
  }

  _id: any;
  expectation: any = {};
  polyLines: any[] = [];
  marksArray: any[] = [];
  service = new google.maps.DistanceMatrixService();

  getTravelDetails(source, dest) {
    let that = this;
    this._id = setInterval(() => {
      if (localStorage.getItem("livepagetravelDetailsObject") != null) {
        if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
          that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
          console.log("expectation: ", that.expectation)
        } else {
          clearInterval(this._id);
        }
      }
    }, 3000)
    that.service.getDistanceMatrix({
      origins: [source],
      destinations: [dest],
      travelMode: 'DRIVING'
    }, that.callback);
    // that.apiCall.stopLoading();
  }

  callback(response, status) {
    let travelDetailsObject;
    if (status == 'OK') {
      var origins = response.originAddresses;
      for (var i = 0; i < origins.length; i++) {
        var results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
          var element = results[j];
          var distance = element.distance.text;
          var duration = element.duration.text;
          travelDetailsObject = {
            distance: distance,
            duration: duration
          }
        }
      }
      localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
    }
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
      this.ngOnDestroy();
      // that.ongoingGoToPoint = undefined;
      // that.ongoingMoveMarker = undefined;
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
      this.someFunc();
    } else {
      if (maptype == 'TERRAIN') {
        this.ngOnDestroy();
        // that.ongoingGoToPoint = undefined;
        // that.ongoingMoveMarker = undefined;
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
        this.someFunc();
      } else {
        if (maptype == 'NORMAL') {
          this.ngOnDestroy();
          // that.ongoingGoToPoint = undefined;
          // that.ongoingMoveMarker = undefined;
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
          this.someFunc();
        }
      }
    }
  }

  someFunc() {
    var runningDevices = [];
    let that = this;
    that._io = undefined;
    that._io = io.connect(this.apiCall.socketUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    that._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    // debugger
    // var paramData = that.data;
    // this.socketInit(paramData);
    if (that.isSuperAdmin || that.isDealer) {

    } else {
      if (that.selectedVehicle === undefined) {
        // setTimeout(() => {
        //   for (var i = 0; i < that.portstemp.length; i++) {
        //     if (that.portstemp[i].status != "Expired") {
        //       // debugger
        //       if (that.portstemp[i].status === "RUNNING") {
        //         if (runningDevices.length <= 5) {
        //           runningDevices.push(that.portstemp[i]);
        //         }
        //       }
        //     }
        //   }
        //   for (var j = 0; j < runningDevices.length; j++) {
        //     console.log("runningDevices[i]: ", runningDevices[j]);
        //     that.socketInit(runningDevices[j]);
        //   }
        // }, 3000);
      } else if (that.selectedVehicle !== undefined) {
        that.socketInit(that.selectedVehicle);
      }

    }
  }

  settings() {
    let that = this;
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: that.deviceDeatils
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);

      var paramData = this.navParams.get("device");
      this.temp(paramData);
    })
  }

  addPOI() {
    let that = this;
    let modal = this.modalCtrl.create(PoiPage, {
      param1: that.allData[that.impkey]
    });
    modal.onDidDismiss((data) => {
      console.log(data)
      let that = this;
      that.showaddpoibtn = false;
    });
    modal.present();
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      that.allData.map.setCameraTarget(that.latlngCenter);
    } else {
      if (type == 'mapHideTraffic') {
        this.ngOnDestroy();
        that.mapHideTraffic = !that.mapHideTraffic;
        if (that.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
          this.someFunc();
        } else {
          that.allData.map.setTrafficEnabled(false);
          this.someFunc();
        }
      } else {
        if (type == 'showGeofence') {
          console.log("Show Geofence")
          if (that.generalPolygon != undefined) {
            that.generalPolygon.remove();
            that.generalPolygon = undefined;
          } else {
            that.callGeofence();
          }
        }
      }
    }
  }

  callGeofence() {
    this.geoShape = [];
    this.apiCall.startLoading().present();
    this.apiCall.getGeofenceCall(this.userdetails._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("geofence data=> " + data.length)
        if (data.length > 0) {
          this.geoShape = data.map(function (d) {
            return d.geofence.coordinates[0];
          });
          for (var g = 0; g < this.geoShape.length; g++) {
            for (var v = 0; v < this.geoShape[g].length; v++) {
              this.geoShape[g][v] = this.geoShape[g][v].reverse()
            }
          }

          for (var t = 0; t < this.geoShape.length; t++) {
            this.drawPolygon(this.geoShape[t])
          }
        } else {
          let alert = this.alertCtrl.create({
            message: 'No gofence found..!!',
            buttons: ['OK']
          });
          alert.present();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  drawPolygon(polyData) {
    let that = this;
    that.geodata = [];
    that.geodata = polyData.map(function (d) {
      return { lat: d[0], lng: d[1] }
    });
    console.log("geodata=> ", that.geodata)

    // let bounds = new LatLngBounds(that.geodata);
    // that.allData.map.moveCamera({
    // target: bounds
    // });
    let GORYOKAKU_POINTS: ILatLng[] = that.geodata;
    console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS)
    that.allData.map.addPolygon({
      'points': GORYOKAKU_POINTS,
      'strokeColor': '#AA00FF',
      'fillColor': '#00FFAA',
      'strokeWidth': 2
    }).then((polygon: Polygon) => {
      // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
      //   console.log("polygon data=> " + param)
      //   console.log("polygon data=> " + param[1])
      // })
      this.generalPolygon = polygon;
    });
  }

  gotoAll() {
    this.navCtrl.setRoot('LivePage');
    localStorage.setItem("CLICKED_ALL", "CLICKED_ALL");
  }

  ngOnInit() {
    if (localStorage.getItem("CLICKED_ALL") != null) {
      this.showMenuBtn = true;
    }
    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") === 'live') {
        this.showMenuBtn = true;
      }
    }
    if (this.socketChnl.length > 0) {
      this.ngOnDestroy();
    }

    this._io = io.connect(this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    this.mapHideTraffic = false;
    // this.drawerHidden = true;
    this.drawerState = DrawerState.Bottom;
    this.onClickShow = false;

    this.showBtn = false;
    this.SelectVehicle = "Select Vehicle";
    this.selectedVehicle = undefined;
    this.userDevices();
  }

  ngOnDestroy() {
    localStorage.removeItem("CLICKED_ALL");

    for (var i = 0; i < this.socketChnl.length; i++)
      this._io.removeAllListeners(this.socketChnl[i]);
    this._io.on('disconnect', () => {
      this._io.open();
    })
  }

  shareLive() {
    var data = {
      id: this.liveDataShare._id,
      imei: this.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: 60 // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getPOIs() {
    this.allData._poiData = [];
    const _burl = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_burl)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.allData._poiData = data;

        this.plotPOI();
      });
  }

  plotPOI() {
    let toast = this.toastCtrl.create({
      message: "Plotting POI's please wait...",
      position: 'middle'
    });
    for (var v = 0; v < this.allData._poiData.length; v++) {
      toast.present();
      if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
        this.allData.map.addMarker({
          title: this.allData._poiData[v].poi.poiname,
          position: {
            lat: this.allData._poiData[v].poi.location.coordinates[1],
            lng: this.allData._poiData[v].poi.location.coordinates[0]
          },
          icon: './assets/imgs/poiFlag.png',
          animation: GoogleMapsAnimation.BOUNCE
        })
      }
    }
    toast.dismiss();
  }

  liveShare() {
    let that = this;
    var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
    that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
  }

  zoomin() {
    let that = this;
    that.allData.map.animateCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  userDevices() {
    var baseURLp;
    let that = this;
    if (that.allData.map != undefined) {
      that.allData.map.remove();
      that.allData.map = that.newMap();
    } else {
      that.allData.map = that.newMap();
    }

    baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
      this.isSuperAdmin = true;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
        this.isDealer = true;
      }
    }

    that.apiCall.startLoading().present();
    that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(resp => {
        that.apiCall.stopLoading();
        // that.portstemp = resp.devices;
        // console.log("list of vehicles :", that.portstemp)
        that.mapData = [];
        that.mapData = resp.devices.map(function (d) {
          if (d.last_location !== undefined) {
            return { lat: d.last_location['lat'], lng: d.last_location['long'] };
          }
        });

        var dummyData;
        dummyData = resp.devices.map(function (d) {
          if (d.last_location === undefined)
            return;
          else {
            return {
              "position": {
                "lat": d.last_location['lat'],
                "lng": d.last_location['long']
              },
              "name": d.Device_Name,
              "icon": that.getIconUrl(d)
            };
          }
        });

        dummyData = dummyData.filter(function (element) {
          return element !== undefined;
        });

        console.log("dummy data: ", dummyData);
        // console.log("dummy data: ", this.dummyData());

        let bounds = new LatLngBounds(that.mapData);
        that.allData.map.moveCamera({
          target: bounds,
          zoom: 10
        })

        // this.innerFunc(that.portstemp);
        // this.addCluster(this.dummyData());
        if (that.isSuperAdmin || that.isDealer) {
          for (var t = 0; t < dummyData.length; t++) {
            console.log("check position: ", dummyData[t].position);
          }
          // that.addCluster(this.dummyData());
          that.addCluster(dummyData);

        } else {

          for (var i = 0; i < resp.devices.length; i++) {
            if (resp.devices[i].status != "Expired") {
              that.socketInit(resp.devices[i]);
            }
          }
          // that.ngOnDestroy();
          that.portstemp = resp.devices;
        }

      },
        err => {
          that.apiCall.stopLoading();
          console.log(err);
        });
  }

  dummyData() {
    return [
      {
        "position": {
          "lat": 21.382314,
          "lng": -157.933097
        },
        "name": "Starbucks - HI - Aiea  03641",
        "address": "Aiea Shopping Center_99-115\nAiea Heights Drive #125_Aiea, Hawaii 96701",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.3871,
          "lng": -157.9482
        },
        "name": "Starbucks - HI - Aiea  03642",
        "address": "Pearlridge Center_98-125\nKaonohi Street_Aiea, Hawaii 96701",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.363402,
          "lng": -157.928275
        },
        "name": "Starbucks - HI - Aiea  03643",
        "address": "Stadium Marketplace_4561\nSalt Lake Boulevard_Aiea, Hawaii 96818",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.3884,
          "lng": -157.9431
        },
        "name": "Starbucks - HI - Aiea  03644",
        "address": "Pearlridge Mall_98-1005\nMoanalua Road_Aiea, Hawaii 96701",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.6507,
          "lng": -158.0652
        },
        "name": "Starbucks - HI - Haleiwa  03645",
        "address": "Pupukea_59-720 Kamehameha Highway\nHaleiwa, Hawaii 96712",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 19.699335,
          "lng": -155.06566
        },
        "name": "Starbucks - HI - Hilo  03646",
        "address": "Border Waiakea Center_315-325\nMakaala Street_Hilo, Hawaii 96720",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 19.698399,
          "lng": -155.064864
        },
        "name": "Starbucks - HI - Hilo  03647",
        "address": "Prince Kuhio Plaza_111\nEast Puainako Street_Hilo, Hawaii 96720",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 19.722763,
          "lng": -155.085598
        },
        "name": "Starbucks - HI - Hilo [D]  03648",
        "address": "Hilo_438 Kilauea Ave_Hilo, Hawaii 96720",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.338183,
          "lng": -157.917579
        },
        "name": "Starbucks - HI - Honolulu  03649",
        "address": "Airport Trade Center_550\nPaiea Street_Honolulu, Hawaii 96819",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.3074,
          "lng": -157.865199
        },
        "name": "Starbucks - HI - Honolulu  03650",
        "address": "Aloha Tower_1 Aloha Tower Drive\nHonolulu, Hawaii 96813",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.30846253,
          "lng": -157.8614898
        },
        "name": "Starbucks - HI - Honolulu  03651",
        "address": "Bishop_1000 Bishop Street #104\nHonolulu, Hawaii 96813",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.311363,
          "lng": -157.863751
        },
        "name": "Starbucks - HI - Honolulu  03652",
        "address": "Central Pacific Bank_220 South King Street\nHonolulu, Hawaii 96813",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.28507546,
          "lng": -157.838214
        },
        "name": "Starbucks - HI - Honolulu  03653",
        "address": "Discovery Bay_1778 Ala Moana Boulevard\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.342733,
          "lng": -158.027408
        },
        "name": "Starbucks - HI - Honolulu  03654",
        "address": "Ewa Beach_91-1401 Fort Weaver Road\nHonolulu, Hawaii 96706",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.28005068,
          "lng": -157.8285093
        },
        "name": "Starbucks - HI - Honolulu  03655",
        "address": "Duty Free Shopper_330 Royal Hawaiian Avenue\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.3115,
          "lng": -157.8649
        },
        "name": "Starbucks - HI - Honolulu  03656",
        "address": "Financial Plaza_130 Merchant Street #111\nHonolulu, Hawaii 96813",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.282048,
          "lng": -157.713041
        },
        "name": "Starbucks - HI - Honolulu  03657",
        "address": "Hawaii Kai Town Center_6700\nKalanianaole Highway_Honolulu, Hawaii 96825",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.291345,
          "lng": -157.848791
        },
        "name": "Starbucks - HI - Honolulu  03658",
        "address": "Hokua_1288 Ala Moana Blvd\nHonolulu, Hawaii 96814",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.335164,
          "lng": -157.868742
        },
        "name": "Starbucks - HI - Honolulu  03659",
        "address": "Kamehameha Shopping Center_1620 North School Street\nHonolulu, Hawaii 96817",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.27852422,
          "lng": -157.7875773
        },
        "name": "Starbucks - HI - Honolulu  03660",
        "address": "Kahala Mall_4211 Waialae Avenue\nHonolulu, Hawaii 96816",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.294372,
          "lng": -157.841963
        },
        "name": "Starbucks - HI - Honolulu  03661",
        "address": "Keeaumoku_678 Keeamoku Street #106\nHonolulu, Hawaii 96814",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.2819,
          "lng": -157.8163
        },
        "name": "Starbucks - HI - Honolulu  03662",
        "address": "Kapahulu Avenue_625 Kapahulu Avenue\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.27608195,
          "lng": -157.7049835
        },
        "name": "Starbucks - HI - Honolulu  03663",
        "address": "Koko Marina_7192 Kalanianaole Highway\nHonolulu, Hawaii 96825",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.3129,
          "lng": -157.8129
        },
        "name": "Starbucks - HI - Honolulu  03664",
        "address": "Manoa Valley_2902 East Manoa Road\nHonolulu, Hawaii 96822",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.293082,
          "lng": -157.847092
        },
        "name": "Starbucks - HI - Honolulu  03665",
        "address": "Macys Ala Moana_1450 Ala Moan Boulevard\nHonolulu, Hawaii 96814",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.341957,
          "lng": -157.929089
        },
        "name": "Starbucks - HI - Honolulu  03666",
        "address": "Moanalua Shopping Center_930 Valkenburgh Street\nHonolulu, Hawaii 96818",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.279503,
          "lng": -157.833101
        },
        "name": "Starbucks - HI - Honolulu  03667",
        "address": "Outrigger Reef_2169 Kalia Road #102\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.282251,
          "lng": -157.828172
        },
        "name": "Starbucks - HI - Honolulu  03668",
        "address": "Ohana Waikiki West_2330 Kuhio Avenue\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.323602,
          "lng": -157.890904
        },
        "name": "Starbucks - HI - Honolulu  03669",
        "address": "Sand Island_120 Sand Island Access Road #4\nHonolulu, Hawaii 96819",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.270016,
          "lng": -157.82381
        },
        "name": "Starbucks - HI - Honolulu  03670",
        "address": "Park Shore Hotel_2856 Kalakaua Avenue\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.289497,
          "lng": -157.842832
        },
        "name": "Starbucks - HI - Honolulu  03671",
        "address": "Sears Ala Moana Center_1450 Ala Moana Blvd.\nHonolulu, Hawaii 96814",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.28201,
          "lng": -157.831087
        },
        "name": "Starbucks - HI - Honolulu  03672",
        "address": "Waikiki Shopping Plaza_2270 Kalakaua Avenue #1800\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.2833,
          "lng": -157.8298
        },
        "name": "Starbucks - HI - Honolulu  03673",
        "address": "Waikiki Trade Center_2255 Kuhio Avenue S-1\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.297,
          "lng": -157.8555
        },
        "name": "Starbucks - HI - Honolulu  03674",
        "address": "Ward Entertainment Center_310 Kamakee Street #6\nHonolulu, Hawaii 96814",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.406095,
          "lng": -157.800761
        },
        "name": "Starbucks - HI - Honolulu  03675",
        "address": "Windward City Shopping Center_45-480 Kaneohe Bay Drive\nHonolulu, Hawaii 96744",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.2829,
          "lng": -157.8318
        },
        "name": "Starbucks - HI - Honolulu  03676",
        "address": "Waikiki Walk_2222 Kalakaua Avenue\nHonolulu, Hawaii 96815",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.293045,
          "lng": -157.852223
        },
        "name": "Starbucks - HI - Honolulu  03677",
        "address": "Ward Gateway_1142 Auahi Street\nHonolulu, Hawaii 96814",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.3067,
          "lng": -157.858444
        },
        "name": "Starbucks - HI - Honolulu [A]  03678",
        "address": "HNL Honolulu Airport_300 Rogers Blvd\nHonolulu, Hawaii 96820",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.891963,
          "lng": -156.477614
        },
        "name": "Starbucks - HI - Kahului  03679",
        "address": "Queen Kaahumanu Center_275 West Kaahuman Avenue #1200 F5\nKahului, Hawaii 96732",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.8843,
          "lng": -156.4583
        },
        "name": "Starbucks - HI - Kahului  03680",
        "address": "Maui Marketplace_270 Dairy Road\nKahului, Hawaii 96732",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.39356972,
          "lng": -157.7403334
        },
        "name": "Starbucks - HI - Kailua  03681",
        "address": "Kailua Village_539 Kailua Road\nKailua, Hawaii 96734",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 19.6512,
          "lng": -155.9944
        },
        "name": "Starbucks - HI - Kailua-Kona  03682",
        "address": "Kona Coast Shopping Center_74-5588 Palani Road\nKailua-Kona, Hawaii 96740",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 19.8434,
          "lng": -155.7652
        },
        "name": "Starbucks - HI - Kamuela  03683",
        "address": "Parker Ranch Center_67-1185 Mamalahoa Highway D108\nKamuela, Hawaii 96743",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.34306,
          "lng": -158.078906
        },
        "name": "Starbucks - HI - Kapolei  03684",
        "address": "Halekuai Center_563 Farrington Highway #101\nKapolei, Hawaii 96707",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.336,
          "lng": -158.0802
        },
        "name": "Starbucks - HI - Kapolei [D]  03685",
        "address": "Kapolei Parkway_338 Kamokila Boulevard #108\nKapolei, Hawaii 96797",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.740343,
          "lng": -156.456218
        },
        "name": "Starbucks - HI - Kihei  03686",
        "address": "Kukui Mall_1819 South Kihei Road\nKihei, Hawaii 96738",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.7575,
          "lng": -156.4559
        },
        "name": "Starbucks - HI - Kihei  03687",
        "address": "Piilani Village Shopping Center_247 Piikea Avenue #106\nKihei, Hawaii 96753",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 19.9338,
          "lng": -155.8422
        },
        "name": "Starbucks - HI - Kohala Coast  03688",
        "address": "Mauna Lani_68-1330 Mauna Lani Drive H-101b\nKohala Coast, Hawaii 96743",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.886244,
          "lng": -156.684697
        },
        "name": "Starbucks - HI - Lahaina  03689",
        "address": "Lahaina Cannery Mall_1221 Honoapiilani Highway\nLahaina, Hawaii 96761",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.8804,
          "lng": -156.6816
        },
        "name": "Starbucks - HI - Lahaina  03690",
        "address": "Lahaina_845 Wainee Street\nLahaina, Hawaii 96761",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.970661,
          "lng": -159.396274
        },
        "name": "Starbucks - HI - Lihue  03691",
        "address": "Kukui Grove_3-2600 Kaumualii Highway #A8\nLihue, Hawaii 96766",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.8818,
          "lng": -156.4633
        },
        "name": "Starbucks - HI - Maui [A]  03692",
        "address": "OGG Kahului Main Concourse_New Terminal Bldg @ Bldg 340\nMaui, Hawaii 96732",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.4585,
          "lng": -158.0178
        },
        "name": "Starbucks - HI - Mililani  03693",
        "address": "Mililani Shopping Center_95-221 Kipapa Drive\nMililani, Hawaii 96789",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.474341,
          "lng": -158.001864
        },
        "name": "Starbucks - HI - Mililani  03694",
        "address": "Mililani Town Center_95-1249 Meheula Parkway\nMililani, Hawaii 96789",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 20.838965,
          "lng": -156.34192
        },
        "name": "Starbucks - HI - Pukalani  03695",
        "address": "Pukalani Foodland_55 Pukalani Street\nPukalani, Hawaii 96768",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.378675,
          "lng": -157.728499
        },
        "name": "Starbucks - HI - Waipahu  03696",
        "address": "Enchanted Lakes_1020 Keolu Drive\nWaipahu, Hawaii 96734",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.39662113,
          "lng": -158.0317397
        },
        "name": "Starbucks - HI - Waipahu  03697",
        "address": "Kunia Shopping Center_94-673 Kupuohi Street\nWaipahu, Hawaii 96797",
        "icon": "assets/markercluster/marker.png"
      },
      {
        "position": {
          "lat": 21.403688,
          "lng": -158.006128
        },
        "name": "Starbucks - HI - Waipahu  03698",
        "address": "Waikele_94-799 Lumiaina Street\nWaipahu, Hawaii 96797",
        "icon": "assets/markercluster/marker.png"
      }
    ];
  }

  getIconUrl(data) {
    let that = this;
    var iconUrl;
    if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
      if (that.plt.is('ios')) {
        iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
      } else if (that.plt.is('android')) {
        iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
      }
    } else {
      if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
        if (that.plt.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
        } else if (that.plt.is('android')) {
          iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
        }
      } else {
        if (that.plt.is('ios')) {
          iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
        } else if (that.plt.is('android')) {
          iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
        }
      }
    }
    return iconUrl;
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  socketInit(pdata, center = false) {
    let that = this;

    that._io.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    that._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

      if (d4 != undefined)

        // console.log('d4 data: ', d4);
        (function (data) {

          if (data == undefined) {
            return;
          }
          console.log("on ping: ", data)
          if (data._id != undefined && data.last_location != undefined) {
            var key = data._id;
            that.impkey = data._id;
            that.deviceDeatils = data;
            let ic = _.cloneDeep(that.icons[data.iconType]);
            if (!ic) {
              return;
            }
            ic.path = null;
            if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
              }
            } else {
              if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
                if (that.plt.is('ios')) {
                  ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                } else if (that.plt.is('android')) {
                  ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
              } else {
                if (data.status.toLowerCase() === 'idling' && data.last_speed === 0) {
                  if (that.plt.is('ios')) {
                    ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                  } else if (that.plt.is('android')) {
                    ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                  }
                } else {
                  if (data.status.toLowerCase() === 'idling' && data.last_speed > 0) {
                    if (that.plt.is('ios')) {
                      ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                    } else if (that.plt.is('android')) {
                      ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                    }
                  } else {
                    if (that.plt.is('ios')) {
                      ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    } else if (that.plt.is('android')) {
                      ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                  }
                }
              }
            }

            // console.log("ic url=> " + ic.url)
            that.vehicle_speed = data.last_speed;
            that.todays_odo = data.today_odo;
            that.total_odo = data.total_odo;
            // that.fuel = data.currentFuel;
            if (that.userdetails.fuel_unit == 'LITRE') {
              that.fuel = data.currentFuel;
            } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
              that.fuel = data.fuel_percent;
            } else {
              that.fuel = data.currentFuel;
            }
            that.last_ping_on = data.last_ping_on;

            if (data.lastStoppedAt != null) {
              var fd = new Date(data.lastStoppedAt).getTime();
              var td = new Date().getTime();
              var time_difference = td - fd;
              var total_min = time_difference / 60000;
              var hours = total_min / 60
              var rhours = Math.floor(hours);
              var minutes = (hours - rhours) * 60;
              var rminutes = Math.round(minutes);
              that.lastStoppedAt = rhours + ':' + rminutes;
            } else {
              that.lastStoppedAt = '00' + ':' + '00';
            }

            that.distFromLastStop = data.distFromLastStop;
            if (!isNaN(data.timeAtLastStop)) {
              that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
            } else {
              that.timeAtLastStop = '00:00:00';
            }
            that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
            that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
            that.last_ACC = data.last_ACC;
            that.acModel = data.ac;
            that.currentFuel = data.currentFuel;
            that.power = data.power;
            that.gpsTracking = data.gpsTracking;
            that.recenterMeLat = data.last_location.lat;
            that.recenterMeLng = data.last_location.long;
            if (data.last_location != undefined) {
              that.getAddress(data.last_location);
            }
            var strStr;
            if (that.allData[key]) {
              strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
              that.socketSwitch[key] = data;
              if (that.allData[key].mark != undefined) {
                // that.allData[key].mark.setTitle();

                that.allData[key].mark.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                  // alert('clicked');
                  setTimeout(() => {
                    that.allData[key].mark.hideInfoWindow();
                  }, 2000)
                });
                that.allData[key].mark.setSnippet(strStr);
                that.allData[key].mark.setIcon(ic);
                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                var temp = _.cloneDeep(that.allData[key].poly[1]);
                that.allData[key].poly[0] = _.cloneDeep(temp);
                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                // that.getAddress(data.last_location);
                that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
              } else {
                return;
              }
            }
            else {
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];

              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

              if (data.last_location != undefined) {
                // that.getAddress(data.last_location);
                strStr = ' Address: ' + that.address + '\n Last Updated: ' + moment(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                // var strStr = data.Device_Name + '\n' + 'Speed:- ' + data.last_speed + '\n' + 'Last Updated:- ' + data.last_ping_on
                that.allData.map.addMarker({
                  title: data.Device_Name,
                  snippet: strStr,
                  position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                  icon: ic,
                }).then((marker: Marker) => {

                  that.allData[key].mark = marker;

                  // if (that.selectedVehicle == undefined) {
                  //   marker.showInfoWindow();
                  // }

                  // if (that.selectedVehicle != undefined) {
                  marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                    .subscribe(e => {
                      if (that.selectedVehicle == undefined) {
                        that.getAddressTitle(marker);
                      } else {
                        that.liveVehicleName = data.Device_Name;
                        // that.drawerHidden = false;
                        that.showaddpoibtn = true;
                        that.drawerState = DrawerState.Docked;
                        that.onClickShow = true;
                      }
                    });
                  // that.getAddress(data.last_location);
                  var speed = data.status == "RUNNING" ? data.last_speed : 0;
                  that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                })
              }
            }

          }
        })(d4)
    })
  }

  getAddress(coordinates) {
    let that = this;
    if (!coordinates) {
      that.address = 'N/A';
      return;
    }
    this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
        that.address = str;
      })
  }

  // saveAddressToServer(address, lat, lng) {
  //   let payLoad = {
  //     "lat": lat,
  //     "long": lng,
  //     "address": address
  //   }
  //   this.apiCall.saveGoogleAddressAPI(payLoad)
  //     .subscribe(respData => {
  //       console.log("check if address is stored in db or not? ", respData)
  //     },
  //       err => {
  //         console.log("getting err while trying to save the address: ", err);
  //       });
  // }

  showNearby(key) {
    // for police stations
    var url, icurl;
    if (key === 'police') {
      if (this.policeButtonColor == '#4369C0') {
        this.policeButtonColor = '#f4f4f4';
        this.policeColor = '#4369C0';
      } else {
        if (this.policeButtonColor == '#f4f4f4') {
          this.policeButtonColor = '#4369C0';
          this.policeColor = '#fff';
        }
      }
      // url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.recenterMeLat + "," + this.recenterMeLng + "&radius=1000&types=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
      url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
      icurl = './assets/imgs/police-station.png';
    } else if (key === 'petrol') {
      if (this.petrolButtonColor == '#4369C0') {
        this.petrolButtonColor = '#f4f4f4';
        this.petrolColor = '#4369C0';
      } else {
        if (this.petrolButtonColor == '#f4f4f4') {
          this.petrolButtonColor = '#4369C0';
          this.petrolColor = '#fff';
        }
      }
      // url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.recenterMeLat + "," + this.recenterMeLng + "&radius=1000&types=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
      url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
      icurl = './assets/imgs/gas-pump.png';
    }

    console.log("google maps activity: ", url);
    this.apiCall.getSOSReportAPI(url).subscribe(resp => {
      console.log(resp.status);
      if (resp.status === 'OK') {
        console.log(resp.results);
        console.log(resp);
        var mapData = resp.results.map(function (d) {
          return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
        })

        let bounds = new LatLngBounds(mapData);

        for (var t = 0; t < resp.results.length; t++) {
          this.allData.map.addMarker({
            title: resp.results[t].name,
            position: {
              lat: resp.results[t].geometry.location.lat,
              lng: resp.results[t].geometry.location.lng
            },
            icon: icurl
          }).then((mark: Marker) => {

          })
        }
        this.allData.map.moveCamera({
          target: bounds
        });
        this.allData.map.setPadding(20, 20, 20, 20);
        this.zoomLevel = 10;
      } else if (resp.status === 'ZERO_RESULTS') {
        if (key === 'police') {
          this.showToastMsg('No nearby police stations found.');
        } else if (key === 'petrol') {
          this.showToastMsg('No nearby petrol pump found.');
        }
      } else if (resp.status === 'OVER_QUERY_LIMIT') {
        this.showToastMsg('Query limit exeed. Please try after some time.');
      } else if (resp.status === 'REQUEST_DENIED') {
        this.showToastMsg('Please check your API key.');
      } else if (resp.status === 'INVALID_REQUEST') {
        this.showToastMsg('Invalid request. Please try again.');
      } else if (resp.status === 'UNKNOWN_ERROR') {
        this.showToastMsg('An unknown error occured. Please try again.');
      }
    })
  }

  showToastMsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    }).present();
  }

  reCenterMe() {
    // console.log("getzoom level: " + this.allData.map.getCameraZoom());
    this.allData.map.moveCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      zoom: this.allData.map.getCameraZoom()
    }).then(() => {

    })
  }

  getAddressTitle(marker) {
    // if(marker)
    console.log("issue coming from getAddressTitle")
    var payload = {
      "lat": marker.getPosition().lat,
      "long": marker.getPosition().lng,
      "api_id": "1"
    }
    var addr;
    this.apiCall.getAddressApi(payload)
      .subscribe((data) => {
        console.log("got address: " + data.results)
        if (data.results[2] != undefined || data.results[2] != null) {
          addr = data.results[2].formatted_address;
        } else {
          addr = 'N/A';
        }
        marker.setSnippet(addr);
      })
  }

  addCluster(data) {
    let markerCluster: MarkerCluster = this.allData.map.addMarkerClusterSync({
      markers: data,
      icons: [
        {
          min: 3,
          max: 9,
          url: "./assets/markercluster/small.png",
          label: {
            color: "white"
          }
        },
        {
          min: 10,
          url: "./assets/markercluster/large.png",
          label: {
            color: "white"
          }
        }
      ]
    });

    markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
      let marker: Marker = params[1];
      marker.setTitle(marker.get("name"));
      marker.setSnippet(marker.get("address"));
      marker.showInfoWindow();
    });

  }

  liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;


      // console.log("issue coming from livetrack 1", mark)
      var lat = 0;
      var lng = 0;
      if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
        lat = mark.getPosition().lat;
        lng = mark.getPosition().lng;
      }


      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        mark.setIcon(icons);
      }
      function _moveMarker() {

        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(new LatLng(lat, lng));
          }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(dest);
          }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  // temp1(data){
  //   this.ngOnDestroy();
  // }

  temp(data) {
    // debugger
    console.log("check selected vehicle: ", this.selectedVehicle)
    console.log("check temp function data: ", data)
    if (data.status == 'Expired') {
      let profileModal = this.modalCtrl.create('ExpiredPage');
      profileModal.present();

      profileModal.onDidDismiss(() => {
        this.selectedVehicle = undefined;
      });

    } else {
      let that = this;
      // that.ngOnDestroy();

      // that._io.on('disconnect', () => {
      //   that._io.open();
      // })

      // that._io = io.connect(that.apiCall.socketUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
      // that._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });

      that.data = data;
      that.liveDataShare = data;
      // that.drawerHidden = true;
      that.drawerState = DrawerState.Bottom;
      that.onClickShow = false;

      if (that.allData.map != undefined) {
        // that.allData.map.clear();
        that.allData.map.remove();
      }

      // console.log("on select change data=> " + JSON.stringify(data));
      debugger
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);
      that.allData = {};
      that.socketChnl = [];
      that.socketSwitch = {};
      if (data) {
        if (data.last_location) {

          let mapOptions = {
            // backgroundColor: 'white',
            controls: {
              compass: true,
              zoom: false,
              mapToolbar: true,
              myLocation: true,
              myLocationButton: false,
            },
            gestures: {
              rotate: false,
              tilt: false
            },
            mapType: that.mapKey
          }
          let map = GoogleMaps.create('map_canvas', mapOptions);
          map.animateCamera({
            target: { lat: 20.5937, lng: 78.9629 },
            zoom: 15,
            duration: 1000,
            padding: 0  // default = 20px
          });

          map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          map.setPadding(20, 20, 20, 20);
          // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          that.allData.map = map;
          // this._io = io.connect(this.apiCall.socketUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
          // this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
          that.socketInit(data);
        } else {
          that.allData.map = that.newMap();
          // this._io = io.connect(this.apiCall.socketUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
          // this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
          that.socketInit(data);
        }

        if (that.selectedVehicle != undefined) {
          // that.drawerHidden = false;
          that.drawerState = DrawerState.Docked;
          that.onClickShow = true;
        }
      }
      that.showBtn = true;
    }
  }
  info(mark, cb) {
    mark.addListener('click', cb);
  }

  setDocHeight() {
    let that = this;
    that.distanceTop = 200;
    that.drawerState = DrawerState.Top;
  }
}
